import pytest


@pytest.fixture(scope="module")
def killians():
    return "Killians"


@pytest.fixture(scope="module")
def molson():
    return "Molson"


@pytest.fixture(scope="module")
def samuel_adams():
    return "Samuel Adams"


@pytest.fixture(scope="module")
def guinness():
    return "Guinness"


@pytest.fixture(scope="module")
def meister_brau():
    return "Meister Brau"


@pytest.fixture(scope="module")
def candidates(killians, molson, samuel_adams, guinness, meister_brau):
    return [killians, molson, samuel_adams, guinness, meister_brau]


@pytest.fixture(scope="module")
def votes(killians, molson, samuel_adams, guinness, meister_brau):
    """
    Return votes that simulate the table below, as obtained from
    the page at http://web.math.princeton.edu/math_alive/6/Lab1/Condorcet.html
    """
    # --------------------------------------
    #               18  12  10  9   4   2
    # --------------------------------------
    # Killians	    5	1	2	4	2	4
    # Molson	    1	5	5	5	5	5
    # Samuel Adams	2	3	4	1	3	3
    # Guinness	    4	4	1	2	4	2
    # Meister Brau	3	2	3	3	1	1

    group_of_18 = (
        {killians: 5, molson: 1, samuel_adams: 2, guinness: 4, meister_brau: 3}
        for _ in range(18)
    )
    group_of_12 = (
        {killians: 1, molson: 5, samuel_adams: 3, guinness: 4, meister_brau: 2}
        for _ in range(12)
    )
    group_of_10 = (
        {killians: 2, molson: 5, samuel_adams: 4, guinness: 1, meister_brau: 3}
        for _ in range(10)
    )
    group_of_09 = (
        {killians: 4, molson: 5, samuel_adams: 1, guinness: 2, meister_brau: 3}
        for _ in range(9)
    )
    group_of_04 = (
        {killians: 2, molson: 5, samuel_adams: 3, guinness: 4, meister_brau: 1}
        for _ in range(4)
    )
    group_of_02 = (
        {killians: 4, molson: 5, samuel_adams: 3, guinness: 2, meister_brau: 1}
        for _ in range(2)
    )

    return [
        *group_of_18,
        *group_of_12,
        *group_of_10,
        *group_of_09,
        *group_of_04,
        *group_of_02,
    ]


@pytest.fixture(scope="module")
def pairwise_results(killians, molson, samuel_adams, guinness, meister_brau):
    return [
        {killians: 37, molson: 18},
        {killians: 26, samuel_adams: 29},
        {killians: 16, guinness: 39},
        {killians: 22, meister_brau: 33},
        {molson: 18, samuel_adams: 37},
        {molson: 18, guinness: 37},
        {molson: 18, meister_brau: 37},
        {samuel_adams: 43, guinness: 12},
        {samuel_adams: 27, meister_brau: 28},
        {guinness: 19, meister_brau: 36},
    ]


@pytest.fixture(scope="module")
def result_table(killians, molson, samuel_adams, guinness, meister_brau):
    return {
        killians: {
            "wins": [molson],
            "losses": [samuel_adams, guinness, meister_brau],
        },
        molson: {
            "wins": [],
            "losses": [killians, samuel_adams, guinness, meister_brau],
        },
        samuel_adams: {
            "wins": [killians, molson, guinness],
            "losses": [meister_brau],
        },
        guinness: {
            "wins": [killians, molson],
            "losses": [samuel_adams, meister_brau],
        },
        meister_brau: {
            "wins": [killians, molson, samuel_adams, guinness],
            "losses": [],
        },
    }


@pytest.fixture(scope="module")
def win_order(killians, molson, samuel_adams, guinness, meister_brau):
    return [meister_brau, samuel_adams, guinness, killians, molson]


@pytest.fixture(scope="module")
def cyclic_result_table():
    return {
        "rock": {
            "wins": ["scissors"],
            "losses": ["paper"],
        },
        "paper": {
            "wins": ["rock"],
            "losses": ["scissors"],
        },
        "scissors": {
            "wins": ["paper"],
            "losses": ["rock"],
        },
    }
